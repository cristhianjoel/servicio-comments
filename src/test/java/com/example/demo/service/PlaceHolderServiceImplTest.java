package com.example.demo.service;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.example.demo.dao.PlaceHolder;
import com.example.demo.model.ModelDto;

@RunWith(MockitoJUnitRunner.class)
public class PlaceHolderServiceImplTest {
	
	@InjectMocks
	private PlaceHolderServiceImpl service;
	
	@Mock
	private PlaceHolder placeholder;
	
	@Test
	public void testGet() {
		
		ModelDto modelOne = new ModelDto();
		modelOne.setId(1);
		modelOne.setPostId(1);
		modelOne.setEmail("email1");
		
		ModelDto modelTwo = new ModelDto();
		modelTwo.setId(2);
		modelTwo.setPostId(2);
		modelTwo.setEmail("email2");
		
		List<ModelDto> ltsModels = new ArrayList();
		ltsModels.add(modelOne);
		ltsModels.add(modelTwo);
		
		Mockito.when(placeholder.getInfo()).thenReturn(ltsModels);
		
		List<String> lts = service.getInfo();
		String text = lts.get(0);
		
		assertEquals(2, lts.size());
		assertTrue(text == "1|1|email1" || text == "2|2|email2");
		
	}
	
}
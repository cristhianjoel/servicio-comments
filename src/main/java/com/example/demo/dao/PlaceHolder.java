package com.example.demo.dao;

import java.util.List;

import com.example.demo.model.ModelDto;

public interface PlaceHolder {
	
	List<ModelDto> getInfo();

}

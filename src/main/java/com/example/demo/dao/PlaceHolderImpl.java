package com.example.demo.dao;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.example.demo.model.Model;
import com.example.demo.model.ModelDto;

@Component
public class PlaceHolderImpl implements PlaceHolder{
	
	@Autowired
	private RestTemplate restTemplate;
	
	private String url = "https://jsonplaceholder.typicode.com/comments";

	@Override
	public List<ModelDto> getInfo() {
		// TODO Auto-generated method stub
		
		Model[] models = restTemplate.getForObject(url, Model[].class);
		
		List<Model> ltsModel = Arrays.asList(models);
		
		return ltsModel.stream().map(model -> build(model)).collect(Collectors.toList());
	}
	
	private ModelDto build(Model model) {
		
		ModelDto modelDto = new ModelDto();
		modelDto.setBody(model.getBody());
		modelDto.setEmail(model.getEmail());
		modelDto.setId(model.getId());
		modelDto.setName(model.getName());
		modelDto.setPostId(model.getPostId());
		
		return modelDto;
	}

	
}

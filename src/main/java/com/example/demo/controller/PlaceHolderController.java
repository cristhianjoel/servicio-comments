package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Response;
import com.example.demo.service.PlaceHolderService;

@RestController
public class PlaceHolderController {
	
	@Autowired
	private PlaceHolderService service;
	
	@GetMapping("/commentsconverted")
	public Response getResponse() {
		
		Response response = new Response();
		response.setData(service.getInfo());
		
		return response;
	}
	
	
}
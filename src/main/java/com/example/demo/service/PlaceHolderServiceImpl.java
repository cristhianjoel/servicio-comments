package com.example.demo.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.dao.PlaceHolder;
import com.example.demo.model.ModelDto;

@Component
public class PlaceHolderServiceImpl implements PlaceHolderService{
	
	@Autowired
	private PlaceHolder placeHolder;

	@Override
	public List<String> getInfo() {
		// TODO Auto-generated method stub
		List<ModelDto> lts = placeHolder.getInfo();
		
		return lts.stream().map(dto -> map(dto)).collect(Collectors.toList());
	}
	
	private String map(ModelDto dto) {
		
		return dto.getPostId()+"|"+dto.getId()+"|"+dto.getEmail();
	}
	
	
	
}